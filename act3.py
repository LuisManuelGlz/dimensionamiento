from math import ceil, floor

contador_de_campos = 0
longitud_del_campo = 0
cabecera_del_campo = 0

###########################################################
#                  Pedimos datos
###########################################################

# se introducen el número total de campos que tendrá la tabla
numero_de_campos = int(input("No. Campos: "))

cabecera_del_registro = int(input("Cabecera del registro: "))

# ciclo for que itera cada campo para definir su longitud y su cabecera
for i in range(numero_de_campos):
    # incrementamos el contador a 1
    contador_de_campos += 1

    # sumamos las longitudes de los campos de la tabla
    longitud_del_campo += int(input(f"\nLongitud del campo {i+1}: "))

    # sumamos las cabeceras de los campos de la tabla
    cabecera_del_campo += int(input(f"Cabecera del campo {i+1}: "))

tamano_del_bloque = int(input("\nTamaño del bloque: "))
cabecera = int(input("Cabecera: "))
pctfree = int(input("PCTFree (0% - 100%): "))
numero_de_registros = int(input("No. de registros: "))

###########################################################
#                  Cálculos
###########################################################

# tamaño total del bloque
registro_por_bloque = tamano_del_bloque # / 1000 * 1024 # puede ser en decimal o binario

# registro por bloque, se busca el espacio libre, así que le restamamos la cabecera
registro_por_bloque_libre = registro_por_bloque - cabecera

# si se introdujo un porcentaje de PCTFree
if pctfree != 0:
    # PctFree convertido
    pctfree = registro_por_bloque_libre * (pctfree / 100)

    # le restamos lo del PCTFree al registro por bloque
    registro_por_bloque_libre = registro_por_bloque_libre - pctfree

# tamaño del registro
tamano_del_registro = registro_por_bloque_libre / (longitud_del_campo + cabecera_del_campo + cabecera_del_registro)

# número de bloques
numero_de_bloques = ceil(numero_de_registros / floor(tamano_del_registro))

# tamaño en bytes
tamano_en_bytes = numero_de_bloques * registro_por_bloque

###########################################################
#                  Resultado
###########################################################

print("\nTamaño en bytes:", tamano_en_bytes)
print("Tamaño en megabytes:", tamano_en_bytes / 1000 / 1000)
